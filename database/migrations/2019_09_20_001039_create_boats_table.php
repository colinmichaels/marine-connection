<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('boats', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->boolean( 'active');
            $table->string('name')->nullable();
            $table->unsignedInteger( 'year')->nullable();
            $table->unsignedInteger( 'manufacturer_id')->nullable();
            $table->text('model_id')->nullable();
            $table->unsignedInteger( 'engine_id');
            $table->unsignedTinyInteger( 'boat_type_id')->nullable();
            $table->unsignedInteger( 'customer_id');
            $table->decimal( 'list_price')->default(0);
            $table->decimal('msrp')->default(0);
            $table->text('serial_number')->nullable();
            $table->text('stock_number')->nullable();
            $table->text('color')->nullable();
            $table->decimal( 'loa')->default(0);
            $table->decimal( 'beam')->default(0);
            $table->decimal('hours')->default(0);
            $table->text('location')->nullable();
            $table->boolean('trailer')->default(false);
            $table->unsignedTinyInteger( 'sale_type_id')->default(0);
            $table->unsignedTinyInteger( 'boat_status_id')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('boats');
    }
}
