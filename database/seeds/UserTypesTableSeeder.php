<?php

use App\UserType;
use Illuminate\Database\Seeder;

class UserTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user_types = array(
            ['name' => 'user'],
            ['name' => 'admin'] ,
            ['name' => 'customer'],
            ['name' => 'client'],
            ['name' => 'sales']
        );

        foreach($user_types as $type){
            $user_type = new UserType;
            $user_type->name = $type['name'];
            $user_type->save();
        }
    }
}
