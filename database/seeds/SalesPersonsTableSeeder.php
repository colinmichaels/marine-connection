<?php

use App\SalesPerson;
use Illuminate\Database\Seeder;

class SalesPersonsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(SalesPerson::class, 25)->create();
    }
}
