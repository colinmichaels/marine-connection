<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\User::class, 100)->create()->each(function($user){
            $user->contact()->save(factory(App\Contact::class)->make(
                [
                    'contactable_id' => $user->id,
                    'contactable_type' => 'App\User'
                ]
            ));
       });
    }
}
