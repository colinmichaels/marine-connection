<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Manufacturer;
use App\BoatModel;
use Faker\Generator as Faker;

$factory->define(Manufacturer::class, function (Faker $faker) {
    return [
            'name' => $faker->company,
            'phone' => $faker->phoneNumber,
            'email' => $faker->companyEmail,
            'website' => $faker->domainName
    ];
});
