<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SalesPerson extends User
{

    public function user(){

        return $this->belongsTo( App\User::class);
    }
}
