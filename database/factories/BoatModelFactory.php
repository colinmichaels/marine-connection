<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\BoatModel;
use Faker\Generator as Faker;

$factory->define(BoatModel::class, function (Faker $faker) {
    return [
        'name' => $faker->jobTitle,
        'number' => $faker->randomNumber(4),
        'year' => $faker->year,
        'manufacturer_id' => $faker->randomNumber(1)
    ];
});
