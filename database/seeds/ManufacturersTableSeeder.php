<?php

use App\Manufacturer;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class ManufacturersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        factory(Manufacturer::class, 50)->create()->each(function($manufacturer) use($faker){
            $manufacturer->models()->save(factory(App\BoatModel::class)->make(
              [
                    'manufacturer_id' => $manufacturer->id,
                    'name' => $faker->domainWord,
                    'number' => $faker->randomNumber(8),
                    'year' => $faker->year

                ]
            ));
            $manufacturer->boats()->save(factory(App\Boat::class)->make(
                [
                    'manufacturer_id' => $manufacturer->id,
                ]
            ));
        });
    }
}
