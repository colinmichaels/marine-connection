<footer>
    <div class="container">
        <div class="d-flex py-5">
            <p>Copyright &copy;{{date('Y')}} <a href="https://colinmichaels.com" title="Colin Michaels">colinmichaels.com</a></p>
        </div>
    </div>
</footer>
