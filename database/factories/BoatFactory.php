<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Boat;
use Faker\Generator as Faker;

$factory->define(Boat::class, function (Faker $faker) {

    $price = $faker->randomNumber(6);
    $msrp = $price * 0.20;

    return [
        'name' => $faker->userName,
        'active' => $faker->boolean,
        'year'  =>$faker->year,
        'manufacturer_id' => App\Manufacturer::all()->random()->id,
        'customer_id' => 0,
        'model_id' => $faker->randomNumber(8),
        'engine_id' => $faker->randomNumber(8),
        'boat_type_id' => 0,
        'list_price' =>  $price,
        'msrp' =>  $msrp,
        'serial_number' => $faker->uuid,
        'stock_number' => $faker->randomNumber(6),
        'color'=> $faker->hexColor,
        'loa' => $faker->numberBetween(15,40),
        'beam' => $faker->numberBetween(4,10),
        'hours' => $faker->numberBetween(0,1000),
        'location' => $faker->city,
        'trailer' => $faker->boolean,
        'sale_type_id' => 0,
        'boat_status_id' => 0
    ];
});
