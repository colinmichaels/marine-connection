<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Contact;
use Faker\Generator as Faker;
use Faker\Provider\en_US\Address;

$factory->define(Contact::class, function (Faker $faker) {
    $faker->addProvider( new Address($faker));
    return [
        'email' => $faker->unique()->safeEmail,
        'phone' => $faker->phoneNumber,
        'cell_phone' => $faker->phoneNumber,
        'address' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->state,
        'country' => 'United States',
        'zip' => $faker->postcode
    ];
});
