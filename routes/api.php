<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::apiResource('boats', 'BoatController');
Route::apiResource('customers', 'CustomerController');
Route::apiResource('engines', 'EngineController');
Route::apiResource('invoices', 'InvoiceController');
Route::apiResource('manufacturers', 'ManufacturerController');
Route::apiResource('salespersons', 'SalesPersonController');


Route::middleware('auth:api')->group(function () {


});
