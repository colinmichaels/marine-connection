<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
    /**
     * Seed the application's database.
     * @return void
     */
    public function run() {
        $this->call( UsersTableSeeder::class )
             ->call( CustomersTableSeeder::class )
             ->call( SalesPersonsTableSeeder::class )
             ->call( UserTypesTableSeeder::class )
             ->call( ManufacturersTableSeeder::class )
             ->call( BoatModelsTableSeeder::class )
             ->call( EnginesTableSeeder::class )
             ->call( PagesTableSeeder::class );
    }
}
