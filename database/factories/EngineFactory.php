<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Engine;
use App\Model;
use Faker\Generator as Faker;

$factory->define(Engine::class, function (Faker $faker) {
    return [
        'manufacturer_id' => App\Manufacturer::all()->random()->id,
        'model_number' => $faker->randomNumber(8),
        'fuel_type' => $faker->boolean
    ];
});
