<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    public static function home(){

        return Page::all()->first();
    }

    public function getPathAttribute(){

        return $this->slug;

    }
}
