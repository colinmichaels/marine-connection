<?php

return [
    'title' => env('APP_NAME'),
    'description' => 'Project Site',
    'author' => 'Colin Michaels',
    'keywords' => 'Developer, Laravel'
];
