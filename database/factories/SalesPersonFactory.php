<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use App\SalesPerson;
use Faker\Generator as Faker;

$factory->define(SalesPerson::class, function (Faker $faker) {
    return [
        'user_id' => App\User::all()->random()->id
    ];
});
