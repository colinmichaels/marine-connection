@extends('layouts.app')
@push('header_scripts')
@endpush
@section('content')
    <div class="container bg-light">
        <div class="row justify-content-center">
            <h1>Here I am</h1>
            @include('sections.card')
            <customer></customer>
            @include('sections.invoice')
        </div>
    </div>
@endsection
@push('footer_scripts')
@endpush
