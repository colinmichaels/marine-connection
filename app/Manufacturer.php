<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Manufacturer extends Model
{

    public function scopeActive($query){

        return $query->whereActive(1)->get();
    }

    public function models(){

        return $this->hasMany( 'App\BoatModel');

    }

    public function boats(){

        return $this->hasMany( 'App\Boat');

    }
}
